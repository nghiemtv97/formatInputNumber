import React from "react";

export default function Product(props) {
  return (
    <div>
      <p>Name:{props.name}</p>
      <p>Price:{props.price}</p>
      <p>Quantity:{props.quantity}</p>
      <button onClick={() => props.onSubmit({
        name:props.name,
        price:props.price,
        quantity:props.quantity
      })}>Thêm giỏ hàng</button>
    </div>
  );
}
