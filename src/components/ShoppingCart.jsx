import React, { useState } from "react";
import Product from "./Product";

export default function ShoppingCart() {
  const [cartItems, setCartItems] = useState([]);
  const [isCheckCart, setIsCheckCart] = useState(false);
  console.log(cartItems, "cartItems");
  const handleToggle = () => {
    setIsCheckCart(!isCheckCart);
  };
  return (
    <>
      {isCheckCart && (
        <div>
          {cartItems.map((item, index) => {
            return (
              <div>
                <p>Name:{item.name}</p>
                <p>Price:{item.price}</p>
                <p>Quantity:{item.quantity}</p>
              </div>
            );
          })}
          <h2>Giá: {cartItems.reduce((sum, i) => (sum += +i.price), 0)} </h2>
        </div>
      )}

      <button onClick={handleToggle}>
        Giỏ hàng <b>{cartItems.length}</b>
      </button>
      <Product
        name="Laptop Dell 3567"
        price="2"
        quantity="6"
        onSubmit={(item) => {
          setCartItems([...cartItems, item]);
        }}
      />
    </>
  );
}
