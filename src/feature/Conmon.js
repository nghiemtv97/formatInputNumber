export const formatBigNumber = (num) => {
  return new Intl.NumberFormat("de-DE").format(num);
};
export const revertBigNumber = (num) => {
  if (num) {
    if (typeof num !== "string") {
      num = String(num);
    }
    num = num.split(".").join("");
    num = num.split(",").join(".");
  }
  return Number(num);
};
