import { useState } from "react";
import reactLogo from "./assets/react.svg";
import viteLogo from "/vite.svg";
import "./App.css";
import ShoppingCart from "./components/ShoppingCart";
import { formatBigNumber, revertBigNumber } from "./feature/Conmon";

function App() {
  const [value, setValue] = useState("");
  const [price, setPrice] = useState("");
  const handleChangePrice = (e) => {
    setValue(formatBigNumber(revertBigNumber(e.target.value)));
  };
  const handleSubmit = () => {
    setPrice(revertBigNumber(value));
  };
  return (
    <>
      {/* <ShoppingCart /> */}
      <p>Gửi lên server: {price}</p>
      <input
        value={value}
        onChange={handleChangePrice}
        placeholder="Nhập giá"
      />
      <p>
        <button style={{ cursor: "pointer" }} onClick={handleSubmit}>
          Submit
        </button>
      </p>
    </>
  );
}

export default App;
